/* eslint-disable */
import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import {StorageApi} from './api/api'
import {GetDirectoryInfoRequest} from '../generated/routes_pb'
Vue.use(Vuex, axios)
const api = new StorageApi()
export default new Vuex.Store({
  state: {
    pathname: '/',
    available_size: '11',
    total_size: '22',
    files: []
  },
  getters: {
    getItems: state => {
      return api.GetDirectoryInfo('/')
    },
    getSlashedPath: state => {
      return state.pathname + '/'
    },
    getPath: state => {
      return state.pathname
    }
  },
  actions: {
    init ({commit}) {
      api.init()
    },
    changePath: async (context, payload) => {
      context.commit('SET_PATHNAME', payload)
    },
    changeFiles: async (context, payload) => {
      context.commit('SET_FILES', payload)
    },
    getFiles ({commit}) {
      let files = api.GetDirectoryInfo().files
      commit('SET_FILES', files)
    },
    getDirectoryInfo () {
      let getRequest = new GetDirectoryInfoRequest()
      console.log(`This pathname: ${state.pathname}`)
      getRequest.setPath(state.pathname)
      this.client.getDirectoryInfo(getRequest, {}, (err, response) => {
        console.log(err)
        let res = response.toObject()
        console.log(res)
        this.$store.dispatch('changeFiles', res.path)
        this.$store.dispatch('changePath', res.path)
      })
    },
    postFile ({dispatch, commit}, newFile) {
      const config = {
      }
      try {
        axios.post('api/files/', newFile, config,
          {
            headers: {
              'Content-Type': 'multipart/form-data'
            }
          })
          .then(res => {
            commit('POST_FILE', newFile)
          })
          .then(res => {
            dispatch('loadFiles')
          })
      } catch (error) {
        console.log(error)
      }
    },
    showInfo ({dispatch}, filename) {
      alert(`The info is loaded`)
    },
    moveFile (filename) {
      alert(`The file is moved`)
    },
    createFile () {
      alert(`The file is created`)
    },
    createDirectory () {
      alert(`The directory is deleted`)
    },
    downloadFile () {
      alert(`The file is downloaded`)
    },
    deleteFile () {
      alert(`The file is deleted`)
    },
    deleteDirectory () {
      alert(`The directory is deleted`)
    },
    copyFile () {
      alert(`The file is copied`)
    }
  },

  mutations: {
    SET_PATHNAME (state, pathname) {
      state.pathname = pathname
    },
    SET_FILES (state, files) {
      state.files = files
    }
  }
})
