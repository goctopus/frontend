// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import 'es6-promise/auto'
import Vuex from 'vuex'
import axios from 'axios'
import store from './store'

Vue.use(Vuex)
Vue.use(Buefy)

Vue.config.productionTip = false

axios.defaults.baseURL = 'http://localhost:8000'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'
axios.defaults.xsrfCookieName = 'csrftoken'

/* eslint-disable no-new */

new Vue({
  el: '#app',
  store,
  components: { App },
  template: '<App/>'

})
