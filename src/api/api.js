const {
  GetFileInfoRequest, GetFileRequest, MoveFileRequest, RenameDirectoryRequest, MoveDirectoryRequest,
  DeleteDirectoryRequest, Directory, InitRequest,
  DeleteFileRequest, File, GetDirectoryInfoRequest, GetFileListRequest, GetDirectoryRequest
} = require('../../generated/routes_pb')
const {StorageServicePromiseClient, NameServiceClient} = require('../../generated/routes_grpc_web_pb')

export class StorageApi {
  constructor (
    name_url = 'http://51.15.93.11:9080',
    storage_url = 'http://51.15.93.11:9080'
  ) {
    this.storageClient = new StorageServicePromiseClient(storage_url, null, null)
    this.nameClient = new NameServiceClient(name_url, null, null)
  }

  init () {
    let request = new InitRequest()
    // eslint-disable-next-line handle-callback-err
    this.nameClient.init(request, {}, (err, response) => {
      console.log('Initialized successfully')
      //
      // let array = new Uint8Array(100)
      // array[42] = 10
      // let request = new File()
      // request.setName('name')
      // request.setSize(10)
      // request.setFile('aaaa')
      // // eslint-disable-next-line handle-callback-err
      // this.client.storeFile(request, {}, (err, response) => {
      //   console.error(err)
      //   console.log('Result store: ', response.getDirectoryInfo())
      // })
    })
  }

  // file
  GetFileInfo (path) {
    const request = GetFileInfoRequest()
    // eslint-disable-next-line handle-callback-err
    this.storageClient.getFileList(request, {}, (err, response) => {
      return {
        'name': response.getName(),
        'size': response.getSize()
      }
    })
  }

  GetFile (uuid) {
    const request = GetFileRequest()
    request.setUuid(uuid)
    // eslint-disable-next-line handle-callback-err
    this.storageClient.getFile(request, {}, (err, response) => {
      return {
        'name': response.getName(),
        'size': response.getSize(),
        'file': response.getFile()
      }
    })
  }

  MoveFile (filePath, newPath) {
    const request = MoveFileRequest()
    request.setFilePath(filePath)
    request.setNewPath(newPath)
    // eslint-disable-next-line handle-callback-err
    this.storageClient.moveFile(request, {}, (err, response) => {
      return {
        'name': response.getName(),
        'size': response.getSize()
      }
    })
  }

  DeleteFile (filePath) {
    const request = DeleteFileRequest()
    request.setFilePath(filePath)
    // eslint-disable-next-line handle-callback-err
    this.storageClient.deleteFile(request, {}, (err, response) => {
      return {
        'name': response.getName(),
        'size': response.getSize()
      }
    })
  }

  StoreFile (name, size, file) {
    const request = File()
    request.setName(name)
    request.setSize(size)
    // eslint-disable-next-line handle-callback-err
    this.nameClient.storeFile(request, {}, (err, response) => {
    })
    request.setFile(file)
    // eslint-disable-next-line handle-callback-err
    this.storageClient.storeFile(request, {}, (err, response) => {
      return {
        'name': response.getName(),
        'size': response.getSize()
      }
    })
  }

  // directory
  GetFileList (directoryPath) {
    const request = GetFileListRequest()
    request.setDirectoryPath(directoryPath)
    // eslint-disable-next-line handle-callback-err
    this.storageClient.storeFile(request, {}, (err, response) => {
      return {
        'name': response.getName(),
        'path': response.getPath(),
        'fileInfo': Array.from(response.files)
      }
    })
  }

  GetDirectoryInfo (name) {
    let request = new GetDirectoryInfoRequest()
    request.setPath(name)
    // eslint-disable-next-line handle-callback-err
    this.nameClient.GetDirectoryInfo(request, {}, (err, response) => {
      this.files = response.toObject().files
      // console.log(this.files)
    })
    // const files = await this.nameClient.getDirectoryInfo(request, {})

    // const files = async getFiles()
    // {
    //   return await
    //   this.nameClient.getDirectoryInfo(request, {})
    // }

    // (err, response) => {
    //   let res = response.array[2].map(function (d) { return {'filename': d[0], 'size': d[1], 'is_file': d[2]} })
    //   console.log(Array.from(res))
    //   // [["name.test",3,true],["go"],[null,3,true]]
    //   // const res = {
    //   //   'name': response['0'],
    //   //   'path': response['1'],
    //   //   'files': response['2']
    //   // }
    //   // console.log(res)
    //   return Array.from(res)
    // }
    return this.files
  }

  GetDirectory (name) {
    const request = GetDirectoryRequest()
    request.setName(name)
    // eslint-disable-next-line handle-callback-err
    this.storageClient.storeFile(request, {}, (err, response) => {
      return {
        'name': response.getName(),
        'path': response.getPath(),
        'fileInfo': response.getFileInfo()
      }
    })
  }

  RenameDirectory (name, newName) {
    const request = RenameDirectoryRequest()
    request.setName(name)
    request.setNewName(newName)
    // eslint-disable-next-line handle-callback-err
    this.storageClient.storeFile(request, {}, (err, response) => {
      return {
        'name': response.getName()
      }
    })
  }

  MoveDirectory (path, newPath) {
    const request = MoveDirectoryRequest()
    request.setName(name)
    request.setNewPath(newPath)
    // eslint-disable-next-line handle-callback-err
    this.storageClient.storeFile(request, {}, (err, response) => {
      return {
        'name': response.getName()
      }
    })
  }

  DeleteDirectory (name) {
    const request = DeleteDirectoryRequest()
    request.setName(name)
    // eslint-disable-next-line handle-callback-err
    this.storageClient.storeFile(request, {}, (err, response) => {
      return {
        'name': response.getName()
      }
    })
  }

  StoreDirectory (name, path, files) {
    const request = Directory()
    request.setName(name)
    request.setPath(path)
    request.setFiles(files)
    // eslint-disable-next-line handle-callback-err
    this.storageClient.storeFile(request, {}, (err, response) => {
      return {
        'name': response.getName()
      }
    })
  }
}

export default {
  StorageApi
}
