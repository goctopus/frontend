# Pyctopus frontend

> Distributed Systems project

## Update all submodules

    git submodule update --init --recursive

## Compile gRPC stubs

    protoc --proto_path=common --js_out=import_style=commonjs,binary:generated/ --grpc-web_out=import_style=commonjs,mode=grpcwebtext:generated/ common/routes.proto
    
## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
