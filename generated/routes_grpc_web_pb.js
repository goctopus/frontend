/**
 * @fileoverview gRPC-Web generated client stub for 
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');

const proto = require('./routes_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.StorageServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.StorageServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.InitRequest,
 *   !proto.InitInfo>}
 */
const methodDescriptor_StorageService_Init = new grpc.web.MethodDescriptor(
  '/StorageService/Init',
  grpc.web.MethodType.UNARY,
  proto.InitRequest,
  proto.InitInfo,
  /**
   * @param {!proto.InitRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.InitInfo.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.InitRequest,
 *   !proto.InitInfo>}
 */
const methodInfo_StorageService_Init = new grpc.web.AbstractClientBase.MethodInfo(
  proto.InitInfo,
  /**
   * @param {!proto.InitRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.InitInfo.deserializeBinary
);


/**
 * @param {!proto.InitRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.InitInfo)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.InitInfo>|undefined}
 *     The XHR Node Readable Stream
 */
proto.StorageServiceClient.prototype.init =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/StorageService/Init',
      request,
      metadata || {},
      methodDescriptor_StorageService_Init,
      callback);
};


/**
 * @param {!proto.InitRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.InitInfo>}
 *     A native promise that resolves to the response
 */
proto.StorageServicePromiseClient.prototype.init =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/StorageService/Init',
      request,
      metadata || {},
      methodDescriptor_StorageService_Init);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.StorageFile,
 *   !proto.StorageFileInfo>}
 */
const methodDescriptor_StorageService_StoreFile = new grpc.web.MethodDescriptor(
  '/StorageService/StoreFile',
  grpc.web.MethodType.UNARY,
  proto.StorageFile,
  proto.StorageFileInfo,
  /**
   * @param {!proto.StorageFile} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.StorageFileInfo.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.StorageFile,
 *   !proto.StorageFileInfo>}
 */
const methodInfo_StorageService_StoreFile = new grpc.web.AbstractClientBase.MethodInfo(
  proto.StorageFileInfo,
  /**
   * @param {!proto.StorageFile} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.StorageFileInfo.deserializeBinary
);


/**
 * @param {!proto.StorageFile} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.StorageFileInfo)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.StorageFileInfo>|undefined}
 *     The XHR Node Readable Stream
 */
proto.StorageServiceClient.prototype.storeFile =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/StorageService/StoreFile',
      request,
      metadata || {},
      methodDescriptor_StorageService_StoreFile,
      callback);
};


/**
 * @param {!proto.StorageFile} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.StorageFileInfo>}
 *     A native promise that resolves to the response
 */
proto.StorageServicePromiseClient.prototype.storeFile =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/StorageService/StoreFile',
      request,
      metadata || {},
      methodDescriptor_StorageService_StoreFile);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.GetFileStorageRequest,
 *   !proto.StorageFile>}
 */
const methodDescriptor_StorageService_GetFile = new grpc.web.MethodDescriptor(
  '/StorageService/GetFile',
  grpc.web.MethodType.UNARY,
  proto.GetFileStorageRequest,
  proto.StorageFile,
  /**
   * @param {!proto.GetFileStorageRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.StorageFile.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.GetFileStorageRequest,
 *   !proto.StorageFile>}
 */
const methodInfo_StorageService_GetFile = new grpc.web.AbstractClientBase.MethodInfo(
  proto.StorageFile,
  /**
   * @param {!proto.GetFileStorageRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.StorageFile.deserializeBinary
);


/**
 * @param {!proto.GetFileStorageRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.StorageFile)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.StorageFile>|undefined}
 *     The XHR Node Readable Stream
 */
proto.StorageServiceClient.prototype.getFile =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/StorageService/GetFile',
      request,
      metadata || {},
      methodDescriptor_StorageService_GetFile,
      callback);
};


/**
 * @param {!proto.GetFileStorageRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.StorageFile>}
 *     A native promise that resolves to the response
 */
proto.StorageServicePromiseClient.prototype.getFile =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/StorageService/GetFile',
      request,
      metadata || {},
      methodDescriptor_StorageService_GetFile);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.StorageFileInfo,
 *   !proto.StorageFileInfo>}
 */
const methodDescriptor_StorageService_RemoveFile = new grpc.web.MethodDescriptor(
  '/StorageService/RemoveFile',
  grpc.web.MethodType.UNARY,
  proto.StorageFileInfo,
  proto.StorageFileInfo,
  /**
   * @param {!proto.StorageFileInfo} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.StorageFileInfo.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.StorageFileInfo,
 *   !proto.StorageFileInfo>}
 */
const methodInfo_StorageService_RemoveFile = new grpc.web.AbstractClientBase.MethodInfo(
  proto.StorageFileInfo,
  /**
   * @param {!proto.StorageFileInfo} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.StorageFileInfo.deserializeBinary
);


/**
 * @param {!proto.StorageFileInfo} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.StorageFileInfo)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.StorageFileInfo>|undefined}
 *     The XHR Node Readable Stream
 */
proto.StorageServiceClient.prototype.removeFile =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/StorageService/RemoveFile',
      request,
      metadata || {},
      methodDescriptor_StorageService_RemoveFile,
      callback);
};


/**
 * @param {!proto.StorageFileInfo} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.StorageFileInfo>}
 *     A native promise that resolves to the response
 */
proto.StorageServicePromiseClient.prototype.removeFile =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/StorageService/RemoveFile',
      request,
      metadata || {},
      methodDescriptor_StorageService_RemoveFile);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.CopyFileRequest,
 *   !proto.StorageFileInfo>}
 */
const methodDescriptor_StorageService_CopyFile = new grpc.web.MethodDescriptor(
  '/StorageService/CopyFile',
  grpc.web.MethodType.UNARY,
  proto.CopyFileRequest,
  proto.StorageFileInfo,
  /**
   * @param {!proto.CopyFileRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.StorageFileInfo.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.CopyFileRequest,
 *   !proto.StorageFileInfo>}
 */
const methodInfo_StorageService_CopyFile = new grpc.web.AbstractClientBase.MethodInfo(
  proto.StorageFileInfo,
  /**
   * @param {!proto.CopyFileRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.StorageFileInfo.deserializeBinary
);


/**
 * @param {!proto.CopyFileRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.StorageFileInfo)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.StorageFileInfo>|undefined}
 *     The XHR Node Readable Stream
 */
proto.StorageServiceClient.prototype.copyFile =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/StorageService/CopyFile',
      request,
      metadata || {},
      methodDescriptor_StorageService_CopyFile,
      callback);
};


/**
 * @param {!proto.CopyFileRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.StorageFileInfo>}
 *     A native promise that resolves to the response
 */
proto.StorageServicePromiseClient.prototype.copyFile =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/StorageService/CopyFile',
      request,
      metadata || {},
      methodDescriptor_StorageService_CopyFile);
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.NameServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.NameServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.InitRequest,
 *   !proto.InitInfo>}
 */
const methodDescriptor_NameService_Init = new grpc.web.MethodDescriptor(
  '/NameService/Init',
  grpc.web.MethodType.UNARY,
  proto.InitRequest,
  proto.InitInfo,
  /**
   * @param {!proto.InitRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.InitInfo.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.InitRequest,
 *   !proto.InitInfo>}
 */
const methodInfo_NameService_Init = new grpc.web.AbstractClientBase.MethodInfo(
  proto.InitInfo,
  /**
   * @param {!proto.InitRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.InitInfo.deserializeBinary
);


/**
 * @param {!proto.InitRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.InitInfo)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.InitInfo>|undefined}
 *     The XHR Node Readable Stream
 */
proto.NameServiceClient.prototype.init =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/NameService/Init',
      request,
      metadata || {},
      methodDescriptor_NameService_Init,
      callback);
};


/**
 * @param {!proto.InitRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.InitInfo>}
 *     A native promise that resolves to the response
 */
proto.NameServicePromiseClient.prototype.init =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/NameService/Init',
      request,
      metadata || {},
      methodDescriptor_NameService_Init);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.HeartbeatRequest,
 *   !proto.HeartbeatResponse>}
 */
const methodDescriptor_NameService_Heartbeat = new grpc.web.MethodDescriptor(
  '/NameService/Heartbeat',
  grpc.web.MethodType.UNARY,
  proto.HeartbeatRequest,
  proto.HeartbeatResponse,
  /**
   * @param {!proto.HeartbeatRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.HeartbeatResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.HeartbeatRequest,
 *   !proto.HeartbeatResponse>}
 */
const methodInfo_NameService_Heartbeat = new grpc.web.AbstractClientBase.MethodInfo(
  proto.HeartbeatResponse,
  /**
   * @param {!proto.HeartbeatRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.HeartbeatResponse.deserializeBinary
);


/**
 * @param {!proto.HeartbeatRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.HeartbeatResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.HeartbeatResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.NameServiceClient.prototype.heartbeat =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/NameService/Heartbeat',
      request,
      metadata || {},
      methodDescriptor_NameService_Heartbeat,
      callback);
};


/**
 * @param {!proto.HeartbeatRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.HeartbeatResponse>}
 *     A native promise that resolves to the response
 */
proto.NameServicePromiseClient.prototype.heartbeat =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/NameService/Heartbeat',
      request,
      metadata || {},
      methodDescriptor_NameService_Heartbeat);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.GetFileRequest,
 *   !proto.FileInfo>}
 */
const methodDescriptor_NameService_GetFileInfo = new grpc.web.MethodDescriptor(
  '/NameService/GetFileInfo',
  grpc.web.MethodType.UNARY,
  proto.GetFileRequest,
  proto.FileInfo,
  /**
   * @param {!proto.GetFileRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.FileInfo.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.GetFileRequest,
 *   !proto.FileInfo>}
 */
const methodInfo_NameService_GetFileInfo = new grpc.web.AbstractClientBase.MethodInfo(
  proto.FileInfo,
  /**
   * @param {!proto.GetFileRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.FileInfo.deserializeBinary
);


/**
 * @param {!proto.GetFileRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.FileInfo)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.FileInfo>|undefined}
 *     The XHR Node Readable Stream
 */
proto.NameServiceClient.prototype.getFileInfo =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/NameService/GetFileInfo',
      request,
      metadata || {},
      methodDescriptor_NameService_GetFileInfo,
      callback);
};


/**
 * @param {!proto.GetFileRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.FileInfo>}
 *     A native promise that resolves to the response
 */
proto.NameServicePromiseClient.prototype.getFileInfo =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/NameService/GetFileInfo',
      request,
      metadata || {},
      methodDescriptor_NameService_GetFileInfo);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.GetFileRequest,
 *   !proto.FileWithServersResponse>}
 */
const methodDescriptor_NameService_GetFile = new grpc.web.MethodDescriptor(
  '/NameService/GetFile',
  grpc.web.MethodType.UNARY,
  proto.GetFileRequest,
  proto.FileWithServersResponse,
  /**
   * @param {!proto.GetFileRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.FileWithServersResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.GetFileRequest,
 *   !proto.FileWithServersResponse>}
 */
const methodInfo_NameService_GetFile = new grpc.web.AbstractClientBase.MethodInfo(
  proto.FileWithServersResponse,
  /**
   * @param {!proto.GetFileRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.FileWithServersResponse.deserializeBinary
);


/**
 * @param {!proto.GetFileRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.FileWithServersResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.FileWithServersResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.NameServiceClient.prototype.getFile =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/NameService/GetFile',
      request,
      metadata || {},
      methodDescriptor_NameService_GetFile,
      callback);
};


/**
 * @param {!proto.GetFileRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.FileWithServersResponse>}
 *     A native promise that resolves to the response
 */
proto.NameServicePromiseClient.prototype.getFile =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/NameService/GetFile',
      request,
      metadata || {},
      methodDescriptor_NameService_GetFile);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.MoveFileRequest,
 *   !proto.FileResponse>}
 */
const methodDescriptor_NameService_MoveFile = new grpc.web.MethodDescriptor(
  '/NameService/MoveFile',
  grpc.web.MethodType.UNARY,
  proto.MoveFileRequest,
  proto.FileResponse,
  /**
   * @param {!proto.MoveFileRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.FileResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.MoveFileRequest,
 *   !proto.FileResponse>}
 */
const methodInfo_NameService_MoveFile = new grpc.web.AbstractClientBase.MethodInfo(
  proto.FileResponse,
  /**
   * @param {!proto.MoveFileRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.FileResponse.deserializeBinary
);


/**
 * @param {!proto.MoveFileRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.FileResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.FileResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.NameServiceClient.prototype.moveFile =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/NameService/MoveFile',
      request,
      metadata || {},
      methodDescriptor_NameService_MoveFile,
      callback);
};


/**
 * @param {!proto.MoveFileRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.FileResponse>}
 *     A native promise that resolves to the response
 */
proto.NameServicePromiseClient.prototype.moveFile =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/NameService/MoveFile',
      request,
      metadata || {},
      methodDescriptor_NameService_MoveFile);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.DeleteFileRequest,
 *   !proto.FileResponse>}
 */
const methodDescriptor_NameService_DeleteFile = new grpc.web.MethodDescriptor(
  '/NameService/DeleteFile',
  grpc.web.MethodType.UNARY,
  proto.DeleteFileRequest,
  proto.FileResponse,
  /**
   * @param {!proto.DeleteFileRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.FileResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.DeleteFileRequest,
 *   !proto.FileResponse>}
 */
const methodInfo_NameService_DeleteFile = new grpc.web.AbstractClientBase.MethodInfo(
  proto.FileResponse,
  /**
   * @param {!proto.DeleteFileRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.FileResponse.deserializeBinary
);


/**
 * @param {!proto.DeleteFileRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.FileResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.FileResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.NameServiceClient.prototype.deleteFile =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/NameService/DeleteFile',
      request,
      metadata || {},
      methodDescriptor_NameService_DeleteFile,
      callback);
};


/**
 * @param {!proto.DeleteFileRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.FileResponse>}
 *     A native promise that resolves to the response
 */
proto.NameServicePromiseClient.prototype.deleteFile =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/NameService/DeleteFile',
      request,
      metadata || {},
      methodDescriptor_NameService_DeleteFile);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.StoreFileRequest,
 *   !proto.FileWithServersResponse>}
 */
const methodDescriptor_NameService_StoreFile = new grpc.web.MethodDescriptor(
  '/NameService/StoreFile',
  grpc.web.MethodType.UNARY,
  proto.StoreFileRequest,
  proto.FileWithServersResponse,
  /**
   * @param {!proto.StoreFileRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.FileWithServersResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.StoreFileRequest,
 *   !proto.FileWithServersResponse>}
 */
const methodInfo_NameService_StoreFile = new grpc.web.AbstractClientBase.MethodInfo(
  proto.FileWithServersResponse,
  /**
   * @param {!proto.StoreFileRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.FileWithServersResponse.deserializeBinary
);


/**
 * @param {!proto.StoreFileRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.FileWithServersResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.FileWithServersResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.NameServiceClient.prototype.storeFile =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/NameService/StoreFile',
      request,
      metadata || {},
      methodDescriptor_NameService_StoreFile,
      callback);
};


/**
 * @param {!proto.StoreFileRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.FileWithServersResponse>}
 *     A native promise that resolves to the response
 */
proto.NameServicePromiseClient.prototype.storeFile =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/NameService/StoreFile',
      request,
      metadata || {},
      methodDescriptor_NameService_StoreFile);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.MoveFileRequest,
 *   !proto.CopyFileResponse>}
 */
const methodDescriptor_NameService_CopyFile = new grpc.web.MethodDescriptor(
  '/NameService/CopyFile',
  grpc.web.MethodType.UNARY,
  proto.MoveFileRequest,
  proto.CopyFileResponse,
  /**
   * @param {!proto.MoveFileRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.CopyFileResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.MoveFileRequest,
 *   !proto.CopyFileResponse>}
 */
const methodInfo_NameService_CopyFile = new grpc.web.AbstractClientBase.MethodInfo(
  proto.CopyFileResponse,
  /**
   * @param {!proto.MoveFileRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.CopyFileResponse.deserializeBinary
);


/**
 * @param {!proto.MoveFileRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.CopyFileResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.CopyFileResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.NameServiceClient.prototype.copyFile =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/NameService/CopyFile',
      request,
      metadata || {},
      methodDescriptor_NameService_CopyFile,
      callback);
};


/**
 * @param {!proto.MoveFileRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.CopyFileResponse>}
 *     A native promise that resolves to the response
 */
proto.NameServicePromiseClient.prototype.copyFile =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/NameService/CopyFile',
      request,
      metadata || {},
      methodDescriptor_NameService_CopyFile);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.GetDirectoryInfoRequest,
 *   !proto.Directory>}
 */
const methodDescriptor_NameService_GetDirectoryInfo = new grpc.web.MethodDescriptor(
  '/NameService/GetDirectoryInfo',
  grpc.web.MethodType.UNARY,
  proto.GetDirectoryInfoRequest,
  proto.Directory,
  /**
   * @param {!proto.GetDirectoryInfoRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.Directory.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.GetDirectoryInfoRequest,
 *   !proto.Directory>}
 */
const methodInfo_NameService_GetDirectoryInfo = new grpc.web.AbstractClientBase.MethodInfo(
  proto.Directory,
  /**
   * @param {!proto.GetDirectoryInfoRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.Directory.deserializeBinary
);


/**
 * @param {!proto.GetDirectoryInfoRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.Directory)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.Directory>|undefined}
 *     The XHR Node Readable Stream
 */
proto.NameServiceClient.prototype.getDirectoryInfo =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/NameService/GetDirectoryInfo',
      request,
      metadata || {},
      methodDescriptor_NameService_GetDirectoryInfo,
      callback);
};


/**
 * @param {!proto.GetDirectoryInfoRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.Directory>}
 *     A native promise that resolves to the response
 */
proto.NameServicePromiseClient.prototype.getDirectoryInfo =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/NameService/GetDirectoryInfo',
      request,
      metadata || {},
      methodDescriptor_NameService_GetDirectoryInfo);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.DeleteDirectoryRequest,
 *   !proto.DirectoryInfo>}
 */
const methodDescriptor_NameService_DeleteDirectory = new grpc.web.MethodDescriptor(
  '/NameService/DeleteDirectory',
  grpc.web.MethodType.UNARY,
  proto.DeleteDirectoryRequest,
  proto.DirectoryInfo,
  /**
   * @param {!proto.DeleteDirectoryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.DirectoryInfo.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.DeleteDirectoryRequest,
 *   !proto.DirectoryInfo>}
 */
const methodInfo_NameService_DeleteDirectory = new grpc.web.AbstractClientBase.MethodInfo(
  proto.DirectoryInfo,
  /**
   * @param {!proto.DeleteDirectoryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.DirectoryInfo.deserializeBinary
);


/**
 * @param {!proto.DeleteDirectoryRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.DirectoryInfo)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.DirectoryInfo>|undefined}
 *     The XHR Node Readable Stream
 */
proto.NameServiceClient.prototype.deleteDirectory =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/NameService/DeleteDirectory',
      request,
      metadata || {},
      methodDescriptor_NameService_DeleteDirectory,
      callback);
};


/**
 * @param {!proto.DeleteDirectoryRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.DirectoryInfo>}
 *     A native promise that resolves to the response
 */
proto.NameServicePromiseClient.prototype.deleteDirectory =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/NameService/DeleteDirectory',
      request,
      metadata || {},
      methodDescriptor_NameService_DeleteDirectory);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.CreateDirectoryRequest,
 *   !proto.DirectoryInfo>}
 */
const methodDescriptor_NameService_StoreDirectory = new grpc.web.MethodDescriptor(
  '/NameService/StoreDirectory',
  grpc.web.MethodType.UNARY,
  proto.CreateDirectoryRequest,
  proto.DirectoryInfo,
  /**
   * @param {!proto.CreateDirectoryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.DirectoryInfo.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.CreateDirectoryRequest,
 *   !proto.DirectoryInfo>}
 */
const methodInfo_NameService_StoreDirectory = new grpc.web.AbstractClientBase.MethodInfo(
  proto.DirectoryInfo,
  /**
   * @param {!proto.CreateDirectoryRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.DirectoryInfo.deserializeBinary
);


/**
 * @param {!proto.CreateDirectoryRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.DirectoryInfo)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.DirectoryInfo>|undefined}
 *     The XHR Node Readable Stream
 */
proto.NameServiceClient.prototype.storeDirectory =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/NameService/StoreDirectory',
      request,
      metadata || {},
      methodDescriptor_NameService_StoreDirectory,
      callback);
};


/**
 * @param {!proto.CreateDirectoryRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.DirectoryInfo>}
 *     A native promise that resolves to the response
 */
proto.NameServicePromiseClient.prototype.storeDirectory =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/NameService/StoreDirectory',
      request,
      metadata || {},
      methodDescriptor_NameService_StoreDirectory);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.AckRequest,
 *   !proto.AckResponse>}
 */
const methodDescriptor_NameService_FileAcknowledge = new grpc.web.MethodDescriptor(
  '/NameService/FileAcknowledge',
  grpc.web.MethodType.UNARY,
  proto.AckRequest,
  proto.AckResponse,
  /**
   * @param {!proto.AckRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.AckResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.AckRequest,
 *   !proto.AckResponse>}
 */
const methodInfo_NameService_FileAcknowledge = new grpc.web.AbstractClientBase.MethodInfo(
  proto.AckResponse,
  /**
   * @param {!proto.AckRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.AckResponse.deserializeBinary
);


/**
 * @param {!proto.AckRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.AckResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.AckResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.NameServiceClient.prototype.fileAcknowledge =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/NameService/FileAcknowledge',
      request,
      metadata || {},
      methodDescriptor_NameService_FileAcknowledge,
      callback);
};


/**
 * @param {!proto.AckRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.AckResponse>}
 *     A native promise that resolves to the response
 */
proto.NameServicePromiseClient.prototype.fileAcknowledge =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/NameService/FileAcknowledge',
      request,
      metadata || {},
      methodDescriptor_NameService_FileAcknowledge);
};


module.exports = proto;

