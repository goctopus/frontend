#!/bin/bash

set -ex

docker network create --driver overlay --attachable tool || true
docker network create --driver overlay --attachable traefik || true

docker stack deploy --compose-file docker-compose.yml && \
docker service ls
